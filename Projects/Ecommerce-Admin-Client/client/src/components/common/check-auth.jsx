import { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

export const CheckAuth = ({ isAuthenticated, user, children }) => {
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (
      !isAuthenticated &&
      !(
        location.pathname.includes("/login") ||
        location.pathname.includes("register")
      )
    ) {
      navigate("/auth/login");
    }

    if (
      isAuthenticated &&
      (location.pathname.includes("/login") ||
        location.pathname.includes("/register"))
    ) {
      if (user?.role === "admin") {
        navigate("/admin/dashboard");
      } else {
        navigate("/shop/home");
      }
    }

    if (
      isAuthenticated &&
      location.pathname.includes("admin") &&
      user?.role !== "admin"
    ) {
      navigate("/unauth-page");
    }

    if (
      isAuthenticated &&
      location.pathname.includes("shop") &&
      user?.role === "admin"
    ) {
      navigate("/admin/dashboard");
    }
  }, [isAuthenticated, user, children]);

  return <>{children}</>;
};
