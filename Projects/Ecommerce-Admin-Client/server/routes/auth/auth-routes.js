const express = require("express");
const router = express.Router();
const authController = require("../../controllers/auth/auth-controller");

router.post("/register", authController.registerUser);
router.post("/login", authController.loginUser);
router.post("/logout", authController.logoutUser);
router.get(
  "/check-auth",
  authController.authMiddleware,
  authController.checkAuth
);

module.exports = router;
