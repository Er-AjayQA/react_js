export const minData = (a, b) => {
  return a - b;
};

const fractionData = (a, b) => {
  return a / b;
};

export { fractionData };
