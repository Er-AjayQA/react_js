// Default Import-Export
import sumData from "./defaultExport.js";

// Names Import-Export
import { minData, fractionData } from "./namedExport.js";

// Alias Import-Export
import { profit as todayProfit } from "./aliasExport.js";

const result1 = sumData(5, 20);
const result2 = minData(5, 20);
const result3 = fractionData(5, 20);

console.log("Sum ==>", result1);
console.log("MinData ==>", result2);
console.log("Fraction ==>", result3);
console.log(`${todayProfit}%`);
