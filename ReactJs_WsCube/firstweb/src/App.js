import "./App.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Footer } from "./Footer";
import { Header } from "./Header";

function App() {
  const name = "Ajay Kumar";
  return (
    <div className="main">
      <Header />
      <Container fluid>
        <Container>
          <Row>
            <Col>
              <h1 className="text-danger">Welcome {name}</h1>
            </Col>
          </Row>
        </Container>
      </Container>
      <Footer />
    </div>
  );
}

export default App;
