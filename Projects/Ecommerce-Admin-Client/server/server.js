const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const authRouter = require("./routes/auth/auth-routes");

// Connecting To App
mongoose
  .connect(
    "mongodb+srv://officialajay2681993:AJAYkumar26@backend.lze3f.mongodb.net/"
  )
  .then(() => {
    console.log("DB is connected successfully ........");
  })
  .catch((error) => {
    console.log(error);
  });

// Configure An App
const app = express();
const PORT = process.env.PORT || 5000;

// Cors Configurations
app.use(
  cors({
    origin: "http://localhost:5173",
    methods: ["get", "post", "put", "delete", "patch"],
    allowedHeaders: [
      "content-type",
      "authorization",
      "cache-controls",
      "Expires",
      "Pragma",
    ],
    credentials: true,
  })
);

// Configure Modules Used By An App
app.use(cookieParser());
app.use(express.json());
app.use("/api/auth", authRouter);

app.listen(PORT, (err) => {
  if (err) console.log(err);
  console.log(`Server is Running at port : ${PORT}`);
});
